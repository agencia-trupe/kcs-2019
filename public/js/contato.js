// console.log('contato');

var $formFooter = $('#form-contato');
$('.submit',$formFooter).click(function(){
    $formFooter.submit();
});
$formFooter.submit(function(e){
    e.preventDefault();
    var $status = $('p.status',$formFooter).removeClass('error');
    $status.html('Enviando...');
    
    head.js(JS_PATH+'/is.simple.validate.js',function(){
        if($formFooter.isSimpleValidate({pre:true})){
            var url  = $formFooter.attr('action')+'.json',
                data = $formFooter.serialize();
            
            $.post(url,data,function(json){
                if(json.error){
                    $status.addClass('error').html(json.error);
                    return false;
                }
                
                // $status.html(json.msg);
                $status.html(json.msg);
                _resetForm('form.contato');
                /*Shop.messageBox('<p class="shop-message-title contato">'+json.msg+'</p>'+
                                '<p class="shop-message-buttons">'+
                                '<a href="#" class="shop-close-message-box fechar-contato single">Fechar</a>'+
                                '</p>');*/
            },'json');
        } else {
            $status.addClass('error').html('* Preencha todos os campos');
        }
    });

    return false;
});

function _resetForm(formSelector){
    $(formSelector).find('input,textarea').val('');
    $(formSelector).find('select option:first-child').attr('selected',true);
}