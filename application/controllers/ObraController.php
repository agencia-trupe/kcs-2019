<?php

class ObraController extends Zend_Controller_Action
{

    public function init()
    {
        $this->table = new Application_Model_Db_Obras();
    }

    public function indexAction()
    {
        $alias = ($this->_hasParam('alias')) ? $this->_getParam('alias') : null;
        if(!$alias) return $this->_redirect('obras');
        $alias = explode('-', $alias);
        $id = array_pop($alias);
        $alias = implode('-', $alias);
        // _d(array($id,$alias));
        
        $obra = _utfRow($this->table->get($id));
        $this->view->obra = $obra;
        // _d($obra);

        if(!$obra || @$obra->status_id == 0 || @$obra->alias!=$alias){
            return $this->_forward('not-found','error','default',array('url'=>URL.'/blog'));
        }

        $fotos = $this->table->getFotosById($obra->id);
        $this->view->fotos = $fotos;
        // _d($fotos);

        $obras = $this->table->fetchAllWithPhoto(
        	'status_id = 1 and t1.id not in ('.$obra->id.') ',
        	'data_cad asc',
        	12,null,
        	array('group'=>'t1.id')
        );
        $this->view->obras = $obras;
        // _d($obras);

        // meta tags
        $this->view->titulo = $obra->titulo;
        $this->view->meta_description = $obra->titulo.': '.Php_Html::toText($obra->body);
        $this->view->meta_canonical = URL.'/obra/'.$obra->alias.'-'.$obra->id;
    }


}

