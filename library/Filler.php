<?

class Filler 
{
	public function chamadasHome()
	{
		return array(
			(object)array(
				'id'   => 1,
				'titulo' => 'RESULTADOS DE EXAMES',
				'link'=> 'http://www.com',
				'body'=> 'Acesse nosso sistema de consulta de Resultados de Exames aqui &raquo;',
				'foto_path'=> 'icone-resultadoexames.png',
			),
			(object)array(
				'id'   => 2,
				'titulo' => 'SOLICITE SEU ORÇAMENTO',
				'link'=> 'http://www.com',
				'body'=> 'RESSONÂNCIA MAGNÉTICA<br>A PARTIR DE R$ 350,00',
				'foto_path'=> 'imgHome-ressonanciamagnetica.png',
			),
			(object)array(
				'id'   => 3,
				'titulo' => 'PEÇA SEU ORÇAMENTO VIA WHATSAPP',
				'link'=> 'http://www.com',
				'body'=> 'Agora você pode fazer seu orçamento também via whatsapp, aproveite!',
				'foto_path'=> 'img-orcawpp.png',
			),
			(object)array(
				'id'   => 4,
				'titulo' => 'AGENDE SEU EXAME',
				'link'=> 'http://www.com',
				'body'=> 'Facilidade, praticidade, foco no ATENDIMENTO HUMANIZADO e excelência nos serviços',
				'foto_path'=> 'icone-agende.png',
			),
			(object)array(
				'id'   => 5,
				'titulo' => 'MENOR PREÇO',
				'link'=> 'http://www.com',
				'body'=> 'Seu exame é mais barato com a ZDI - faça um orçamento',
				'foto_path'=> 'icone-menorpreco.png',
			),
			(object)array(
				'id'   => 6,
				'titulo' => 'PERTO DE VOCÊ',
				'link'=> 'http://www.com',
				'body'=> 'Unidade Belém:<br>Rua Padre Adelino 735<br>Belém - São Paulo, SP',
				'foto_path'=> 'icone-pertovoce.png',
			),
		);
	}

	public static function menuFooter()
	{
		return array(
			0 => array(
				'servico-1' => array('label'=>'Serviço 1','uri'=>'#'),
				'servico-2' => array('label'=>'Serviço 2','uri'=>'#'),
				'servico-3' => array('label'=>'Serviço 3','uri'=>'#'),
				'servico-4' => array('label'=>'Serviço 4','uri'=>'#'),
				'servico-5' => array('label'=>'Serviço 5','uri'=>'#'),
				'servico-6' => array('label'=>'Serviço 6','uri'=>'#'),
				'servico-7' => array('label'=>'Serviço 7','uri'=>'#'),
				'servico-8' => array('label'=>'Serviço 8','uri'=>'#'),
				'servico-9' => array('label'=>'Serviço 9','uri'=>'#'),
				'servico-10' => array('label'=>'Serviço 10','uri'=>'#'),
				'servico-11' => array('label'=>'Serviço 11','uri'=>'#'),
				'servico-12' => array('label'=>'Serviço 12','uri'=>'#'),
				'servico-13' => array('label'=>'Serviço 13','uri'=>'#'),
			),
			1 => array(
				'servico-14' => array('label'=>'Serviço 14','uri'=>'#'),
				'servico-15' => array('label'=>'Serviço 15','uri'=>'#'),
				'servico-16' => array('label'=>'Serviço 16','uri'=>'#'),
				'servico-17' => array('label'=>'Serviço 17','uri'=>'#'),
				'servico-18' => array('label'=>'Serviço 18','uri'=>'#'),
				'servico-19' => array('label'=>'Serviço 19','uri'=>'#'),
				'servico-20' => array('label'=>'Serviço 20','uri'=>'#'),
				'servico-21' => array('label'=>'Serviço 21','uri'=>'#'),
				'servico-22' => array('label'=>'Serviço 22','uri'=>'#'),
				'servico-23' => array('label'=>'Serviço 23','uri'=>'#'),
				'servico-24' => array('label'=>'Serviço 24','uri'=>'#'),
				'servico-25' => array('label'=>'Serviço 25','uri'=>'#'),
				'servico-26' => array('label'=>'Serviço 26','uri'=>'#'),
			),
		);
	}

}