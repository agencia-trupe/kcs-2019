<?php

class Application_Model_Db_Obras extends ZendPlugin_Db_Table 
{
    protected $_name = "obras";
    protected $_foto_join_table = 'obras_fotos'; // tabela de relação para registros de fotos
    protected $_foto_join_table_field = 'obra_id'; // campo de relação para registros da tabela principal
    protected $_foto_join_field = 'foto_id'; // campo para dar join com foto.id
    protected $_arquivo_join_table = 'obras_arquivos'; // tabela de relação para registros de arquivos
    protected $_arquivo_join_table_field = 'obra_id'; // campo de relação para registros da tabela principal
    protected $_arquivo_join_field = 'arquivo_id'; // campo para dar join com arquivo.id
    
    /**
     * Retorna registro por alias
     */
    public function getByAlias($alias)
    {
        return $this->fetchRow('alias = "'.$alias.'"');
    }

    /**
     * Retorna as fotos do da obra
     *
     * @param int $id - id da obra
     *
     * @return array - rowset com fotos da obra
     */
    public function getFotosById($id=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('obras_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            // ->order('tf.id asc');
            ->order(array('f.flag desc','tf.id asc'));
        
        if($id) $select->where('obra_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }

    /**
     * Retorna as arquivos do da obra
     *
     * @param int $id - id da obra
     *
     * @return array - rowset com arquivos da obra
     */
    public function getArquivosById($id=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('obras_arquivos as tf')
            ->join('arquivos as f','f.id=tf.arquivo_id')
            ->order('tf.id asc');
        
        if($id) $select->where('obra_id in ('.$id.')');
        
        $arquivos = $select->query()->fetchAll();
        
        array_walk($arquivos,'Func::_arrayToObject');
        
        return $arquivos;
    }

    /**
     * Adiciona urls já formatadas ao rowset
     */
    public function parseUrls($rows)
    {
        $area = 'obra';
        // $area = 'blog/post';
        
        foreach($rows as $row) {
            $a = ((bool)@$row->categoria) ? 
                 str_replace('/post','/'.$row->categoria->alias,$area) : 
                 $area;
            $row->_url = URL.'/'.$a.'/'.$row->alias.'-'.$row->id;
        }
        
        return $rows;
    }

    /**
     * Adiciona dados da categoria ao rowset
     */
    public function parseCateg($rows)
    {
        $ids = array('1');
        foreach($rows as $row) {
            if((bool)$row->categoria_id) $ids[] = $row->categoria_id;
            $row->categoria = null;
        }
        if(!count($ids)) return $rows;

        $ids = array_unique($ids);
        $categs = $this->q('select * from categorias_obras where id in ('.implode(',', $ids).')');

        foreach($rows as $row)
            foreach($categs as $categ)
                if($row->categoria_id == $categ->id) $row->categoria = $categ;

        return $rows;
    }

    /**
     * Retorna as videos da obra
     *
     * @param int  $id        - id do obra
     * @param int  $limit     - limite da lista
     * @param bool $rand      - ordenação randômica?
     *
     * @return array - rowset com sugestoes do obra
     */
    public function getVideos($id,$limit=null,$rand=false)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = array();

        $select->from('obras_videos as pe',array('*'))
            ->where('pe.obra_id = '.$id)
            ->order($rand ? new Zend_Db_Expr('RAND()') : array('pe.id'))
            // ->group('t.id')
            ->limit($limit);

        $_rows = $select->query()->fetchAll();
        foreach($_rows as $k=>&$v) $rows[] = Is_Array::toObject(Is_Array::utf8All($v));

        return $rows;
    }
    
}