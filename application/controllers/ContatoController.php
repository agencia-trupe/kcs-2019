<?php

class ContatoController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        /* Initialize action controller here */
        $this->view->meta_description = 'Contato '.SITE_TITLE;
        $this->messenger = new Helper_Messenger();

        $this->paginas = new Application_Model_Db_Paginas();
    }

    public function indexAction()
    {
        $pagina = $this->paginas->getPagina(3);
        $this->view->pagina = $pagina;
        $this->view->titulo = $pagina->titulo2;
        // _d($pagina);
    }

    public function enviarOrcamentoAction()
    {
        return $this->enviarAction();
    }
    
    public function enviarAction()
    {
        $r = $this->getRequest();

        if(!$this->isAjax() && !$r->isPost()){
            $this->messenger->addMessage('Requisição inválida','error');
            return $this->_redirect('contato');
        }
        if(!$r->isPost()) return array('error'=>'Requisição inválida');
        
        $form = new Application_Form_Contato();
        $post = $r->getPost();
        // _d($post);
        $assunto = (bool)trim($r->getParam('assunto')) ? ' - '.trim($r->getParam('assunto')) : '';
        // $ass = strtolower(substr(Is_Str::toUrl($r->getParam('assunto')),0,3));
        // _d($ass);
        
        if($form->isValid($post)||1){ // valida post
            $html = "<h1>Contato".$assunto."</h1>". // monta html
                    nl2br($r->getParam('mensagem'))."<br/><br/>".
                    "<b>Nome:</b> ".$r->getParam('nome')."<br/>".
                    "<b>E-mail:</b> <a href='mailto:".
                    $r->getParam('email')."'>".$r->getParam('email').
                    "</a><br/>".
                    "<b>Telefone:</b> ".$r->getParam('telefone')."<br/>";
                    // "<b>Celular:</b> ".$r->getParam('celular');

            // if($ass=='orc'){
            //     $html.= "<b>Nome:</b> ".$r->getParam('nome')."<br/>";
            // } else {
            //     $html.= nl2br($r->getParam('mensagem'))."<br/><br/>";
            // }
            // _d($html);
            
            try { // tenta enviar o e-mail
                if(APP_ENV!='development1') Trupe_Kcs_Mail::sendWithReply(
                    @$post['email'],
                    @$post['nome'],
                    'Contato'.$assunto,
                    $html
                );
                return array('msg'=>'Mensagem enviada com sucesso!');
            } catch(Exception $e){
                return array('error'=>$e->getMessage());
            }
        }
        
        return array('error'=>'* Preencha todos os campos');
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }


}