<?php

class ClientesController extends Zend_Controller_Action
{

    public function init()
    {
        $this->clientes = new Application_Model_Db_Clientes2();
    }

    public function indexAction()
    {
        $rows = $this->clientes->fetchAllWithPhoto(
        	'status_id=1',
        	'ordem'
        );
        $this->view->rows = $rows;
        // _d($rows);
    }


}

