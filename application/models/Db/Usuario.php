<?php
/**
 * Modelo da tabela de usuarios
 */
class Application_Model_Db_Usuario extends Zend_Db_Table 
{
    protected $_name = "usuarios";
    protected $_login_column = "login";
    
    protected $_dependentTables = array('Aluno_Model_Pessoa');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Pessoa' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Pessoa',
            'refColumns'    => 'usuario_id'
        )
    );
    
    // Finds
    public function findByLoginAtivo($email,$password)
    {
        $rows = $this->fetchAll($this->select()
            ->where($this->_login_column.' = ?',$email)
            ->where('senha = ?',md5($password))
            ->where("status_id = ?",1)
            ->limit(1,0));
        return count($rows) ? $rows->current() : false;
    }
    
    public function findByLogin($email,$password,$md5=true)
    {
        $rows = $this->fetchAll($this->select()
            ->where($this->_login_column.' = ?',$email)
            ->where('senha = ?',$md5 ? md5($password) : $password)
            ->limit(1,0));
        return count($rows) ? $rows->current() : false;
    }
    
    public function findByEmail($email)
    {
        //$rows = $this->fetchAll($this->select()->where('email = ?',$email)->limit(1));
        //return count($rows) ? $rows->current() : false;
        return $this->fetchRow('email = "'.$email.'"');
    }
}