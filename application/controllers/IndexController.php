<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $this->chamadas = new Application_Model_Db_Promocoes();
        $this->banners = new Application_Model_Db_Destaques();
    }

    public function indexAction()
    {
        $banners = $this->banners->fetchAllWithPhoto('status_id=1','ordem');
        $this->view->banners = $banners;
        // _d($banners);
        
        $chamadas = $this->chamadas->fetchAllWithPhoto('status_id=1','ordem');
        $this->view->chamadas = $chamadas;
        // _d($chamadas);
    }

    public function mailAction()
    {
        try{
            Trupe_Rmw_Mail::send('patrick.trupe@gmail.com','Trupe','Confirmação','Confirmar leitura do texto');
            echo "ok";
        } catch(Exception $e){
            echo $e->getMessage();
        }
        exit();
    }


}