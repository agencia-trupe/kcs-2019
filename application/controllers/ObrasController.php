<?php

class ObrasController extends Zend_Controller_Action
{

    public function init()
    {
        $this->obras = new Application_Model_Db_Obras();
    }

    public function indexAction($limit=null,$offset=null)
    {
        $obras = $this->obras->fetchAllWithPhoto(
        	'status_id = 1',
        	'data_cad asc',
        	$limit,$offset,
        	array('group'=>'t1.id')
        );
        $this->view->obras = $obras;
        // _d($obras);
    }


}

