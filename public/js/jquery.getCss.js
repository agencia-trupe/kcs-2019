$.extend({
    getCss:function(url,media){
        if($.browser.msie){
            $.get(url,function(css){
                $('body').append('<style type="text/css">'+css+'</style>');
            });
        } else {
            $($("<link />")).attr({href:url,media:media||"screen",type:"text/css",rel:"stylesheet"}).appendTo("body");
        }
    }
});