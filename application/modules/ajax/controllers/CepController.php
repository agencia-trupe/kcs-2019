<?php

class Ajax_CepController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }
    
    function validateCep() {
        $cepstr = $this->_getParam('cep');
        if (strlen($cepstr) != 8) return false;
        for($i=0;$i<8;$i++) {
            $dig = ord($cepstr[$i]);
            if (($dig < 48) || ($dig > 57)) return false;
        }
        return $cepstr;
    }
    
    function buscaAction()
    {
        $cep = $this->validateCep();
        if ($cep){ $endereco = $this->busca($cep); }
        
        if (($cep) && isset($endereco['resultado']) /*($endereco['resultado'] > 0)*/){
            $err = 0;
            $end = urlencode($endereco['tipo_logradouro'] . ' ' . $endereco['logradouro']);
            $bai = urlencode($endereco['bairro']);
            $cid = urlencode($endereco['cidade']);
            $est = $endereco['uf'];
        } else { // CEP não encontrado
            $err = 1;
            $end = "";
            $bai = "";
            $cid = "";
            $est = "";
        }
        
        $arr = array('err'=>$err,'end'=>$end,'bai'=>$bai,'cid'=>$cid,'est'=>$est);
        //$res = json_encode($arr);
        //print $res;
        return $arr;
    }
    
    function busca($cep)
    {  
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://republicavirtual.com.br/web_cep.php?cep='.urlencode($cep).'&formato=query_string');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $x = curl_exec($ch);
        curl_close($ch);
        
        if($x === false){  
            $x = "";
        }  
        parse_str($x, $retorno);   
        return $retorno;  
    }
}

